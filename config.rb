
module Config

  cattr_accessor :brand
  cattr_accessor :platform
  cattr_accessor :language
  cattr_accessor :environment

end