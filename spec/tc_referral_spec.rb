require_relative '../navigation'
require_relative 'spec_helper'

describe 'TC_REFERRAL' do

  before(:all) do
    @navigation = Navigation.new
    @homePage = @navigation.goToHomePage
    facebookPage = @navigation.goToFacebookPage
    facebookPage.facebookLogin
    @accountPage = @navigation.goToAccountPage
    @accountPage.closeAccountPopUp
  end

  before(:each) do
    @referralPage = @navigation.goToReferralPage
  end

  after(:each) do
    @referralPage.validClosePopUp
  end

  it 'Share Categories' do
    @referralPage.loopAllShareCategory
  end

end