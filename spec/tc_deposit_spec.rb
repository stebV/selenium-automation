require_relative '../navigation'
require_relative 'spec_helper'

describe 'TC_DEPOSIT' do

  before(:all) do
    @navigation = Navigation.new
    @homePage = @navigation.goToHomePage
    facebookPage = @navigation.goToFacebookPage
    facebookPage.facebookLogin
    @accountPage = @navigation.goToAccountPage
    @accountPage.closeAccountPopUp
  end

  before(:each) do
    @depositPage = @navigation.goToDepositPage
  end

  after(:each) do
    @depositPage.validClosePopUp
  end

  it 'Empty State' do
    @depositPage.clickSubmit
    @depositPage.validDepositErrorMessage(:empty_state)
  end

  it 'Loop Click Amount' do
    @depositPage.loopAllAmount
  end

  it 'Paypal Deposit' do
    @depositPage.validStateSelectBox("Alabama", "AL")
    @depositPage.clickAndDisplaySubmit
    @depositPage.validPaypalAmount
    @depositPage.clickPaypalReturn
    @navigation.goToDepositPage
  end

end