require "rubygems"
require 'selenium-webdriver'
require "bundler/setup"
require 'rspec'
require 'capybara'
require "capybara/rspec"
require 'capybara/dsl'

Capybara.default_max_wait_time = 3

Capybara.register_driver :chrome do |app|
  Capybara::Selenium::Driver.new(app, :browser => :chrome)
end

RSpec.configure do |config|
  config.include Capybara::DSL
end