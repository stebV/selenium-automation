require_relative '../navigation'
require_relative 'spec_helper'

describe 'TC_SIGNUP' do

  before(:all) do
    @navigation = Navigation.new
  end

  before(:each) do
    @homePage = @navigation.goToHomePage
    @signupPage = @navigation.goToSignupPage
  end

  after(:each) do
    @navigation.resetSession
  end

  it "Login Button" do
    @signupPage.clickLogin
  end

  it "Invalid E-Mail Input" do
    @signupPage.fillInputSignupFromHash(:invalid_type, :valid)
    @signupPage.clickSignupSubmitButton
    @signupPage.validSignupErrorMessage(:invalid_email)
  end

  it "Invalid Signup" do
    @signupPage.fillInputSignupFromHash(:invalid, :valid)
    @signupPage.clickSignupSubmitButton
    @signupPage.validSignupErrorMessage(:invalid_account)
  end

  it "Facebook Signup" do
    @facebookPage = @navigation.goToFacebookPage
    @facebookPage.facebookSignup
  end

  it "Valid Signup" do
    @signupPage.validFillInputSignup
    @signupPage.clickSignupSubmitButton
    @accountPage = @navigation.goToAccountPage
    @accountPage.closeAccountPopUp
    @homePage.clickAccountLogout
  end

  it "Valid Login" do
    @signupPage.validClosePopUp
    @loginPage = @navigation.goToLoginPage
    @loginPage.fillInputLogin(*@signupPage.getArraySignupEmailAndPassowrd)
    @loginPage.clickLoginSubmitButton
    @accountPage = @navigation.goToAccountPage
    @accountPage.closeAccountPopUp
    @homePage.clickAccountLogout
  end

end