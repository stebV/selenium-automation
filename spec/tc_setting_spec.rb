require_relative '../navigation'
require_relative 'spec_helper'

describe 'TC_SETTINGS' do

  before(:all) do
    @navigation = Navigation.new
    @homePage = @navigation.goToHomePage
    facebookPage = @navigation.goToFacebookPage
    facebookPage.facebookLogin
    @accountPage = @navigation.goToAccountPage
    @accountPage.closeAccountPopUp
  end

  before(:each) do
    @settingPage = @navigation.goToSettingPage
  end

  after(:each) do
    @settingPage.validClosePopUp
  end

  it 'Change State' do
    @settingPage.loopAllCategories
  end

end