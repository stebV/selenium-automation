require_relative '../navigation'
require_relative 'spec_helper'

describe "TC_ACCOUNT" do

  before(:all) do
    @navigation = Navigation.new
    @homePage = @navigation.goToHomePage
    facebookPage = @navigation.goToFacebookPage
    facebookPage.facebookLogin
    # loginPage = @navigation.goToLoginPage
    # loginPage.fillInputLogin(:valid, :valid)
    # loginPage.clickLoginSubmitButton
    @balanceAmount = @homePage.getBalanceAmount
    @accountPage = @navigation.goToAccountPage
    @accountPage.redoDefaultUsername
    @accountPage.closeAccountPopUp
  end

  before(:each) do
    @navigation.goToAccountPage
  end

  after(:each) do
    @navigation.refreshPage
  end

  it "Information Fields" do
    @accountPage.validEmailTextFieldFromHash
    @accountPage.validSupportEmailLinkFromHash
    @accountPage.validBalanceTextField(@balanceAmount)
  end

  it "Change Username - Add Template" do
    @accountPage.changeUsernameTemplate("_1Qa@#$%^&*")
    @accountPage.clickChangeUsernameSave
    @navigation.refreshPage
    accountMenuUsername = @homePage.getAccountMenuUsername
    @navigation.goToAccountPage
    @accountPage.validUsernameTextField(accountMenuUsername)
  end

  it "Change Username - Boundary" do
    @accountPage.clickChangeUsername
    @accountPage.fillInputChangeUsername("12345")
    @accountPage.clickChangeUsernameSave(disappear: false)
    @accountPage.validUsernameInputErrorMessage
    @accountPage.clickChangeUsernameCancel
    @accountPage.validUsernameInputErrorMessageDisappear
  end

  it "Change Username - Empty" do
    @accountPage.clickChangeUsername
    @accountPage.validAlertEmptyInput
  end

  it "Transactions" do
    @accountPage.clickTransactions
    @accountPage.validClosePopUp
  end

  it "Withdrawal" do
    @accountPage.clickWithdrawal
    @accountPage.clickWithdrawalPaypal
    @accountPage.clickWithdrawalUploadID
    @accountPage.clickWithdrawalUploadIDCancel
    @accountPage.fillInputWithdrawalAmount("123")
    @accountPage.clickWithdrawalCancel
  end

  it "Deposit" do
    @accountPage.clickDeposit
    @accountPage.validClosePopUp
  end

  end
