require_relative '../navigation'
require_relative 'spec_helper'

describe "TC_LOGIN" do

  before(:all) do
    @navigation = Navigation.new
  end

  before(:each) do
    @homePage = @navigation.goToHomePage
    @loginPage = @navigation.goToLoginPage
  end

  after(:each) do
    @navigation.resetSession
  end

  it "Signup Button" do
    @loginPage.clickSignup
  end

  it "Invalid Forgot Password" do
    @loginPage.clickForgotPasswordButton
    @loginPage.fillInputForgotPassword(:invalid)
    @loginPage.clickForgotPasswordSendButton
    @loginPage.validForgotPasswordErrorMessage(:invalid_email)
  end

  it "Invalid E-Mail Input Forgot Password" do
    @loginPage.clickForgotPasswordButton
    @loginPage.fillInputForgotPassword(:invalid_type)
    @loginPage.clickForgotPasswordSendButton
    @loginPage.validForgotPasswordErrorMessage(:invalid_email_type)
  end

  it "Valid Forgot Password" do
    @loginPage.clickForgotPasswordButton
    @loginPage.fillInputForgotPassword(:valid)
    @loginPage.clickForgotPasswordSendButton
    @loginPage.validForgotPasswordSendMessage
  end

  it "Invalid E-Mail Input" do
    @loginPage.fillInputLoginFromHash(:invalid_type, :valid)
    @loginPage.clickLoginSubmitButton
    @loginPage.validLoginErrorMessage(:invalid_email)
  end

  it "Invalid Login" do
    @loginPage.fillInputLoginFromHash(:invalid, :valid)
    @loginPage.clickLoginSubmitButton
    @loginPage.validLoginErrorMessage(:invalid_account)
  end

  it "Valid Login" do
    @loginPage.fillInputLoginFromHash(:valid, :valid)
    @loginPage.clickLoginSubmitButton
    @accountPage = @navigation.goToAccountPage
    @accountPage.closeAccountPopUp
    @homePage.clickAccountLogout
  end

  it "Facebook Login" do
    @facebookPage = @navigation.goToFacebookPage
    @facebookPage.facebookLogin
    @accountPage = @navigation.goToAccountPage
    @accountPage.closeAccountPopUp
    @homePage.clickAccountLogout
  end

  end
