require_relative '../navigation'
require_relative 'spec_helper'

describe 'TC_PROMO' do

  before(:all) do
    @navigation = Navigation.new
    @homePage = @navigation.goToHomePage
    facebookPage = @navigation.goToFacebookPage
    facebookPage.facebookLogin
    @accountPage = @navigation.goToAccountPage
    @accountPage.closeAccountPopUp
  end

  before(:each) do
    @promoPage = @navigation.goToPromoPage
  end

  after(:each) do
    @promoPage.validClosePopUp
  end

  it 'Empty Code' do
    @promoPage.fillInputCode("")
    @promoPage.clickSubmit
    @promoPage.validCodeErrorMessage(:empty)
  end

  it 'Invalid Code' do
    @promoPage.fillInputCode("123")
    @promoPage.clickSubmit
    @promoPage.validCodeErrorMessage(:invalid)
  end

end