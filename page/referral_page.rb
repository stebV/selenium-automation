require_relative 'page'

class ReferralPage < Page

  def loopAllShareCategory
    getShareCatgeoryElements.each do |element|
      validClickShareCategory(element)
    end
  end

  def getShareCatgeoryElements
    self.getElementChildren(:referral_category_container)
  end

  def validClickShareCategory(element)
    self.validClick(element, element: true)
    self.validDisplayShare(getShareCategoryName(element))
  end

  def getShareCategoryName(element)
    getElementText(element, element: true).downcase
  end

  def validDisplayShare(name)
    self.validDisplay(getShareSelector(name))
  end

  def getShareSelector(name)
    format('referral_category_container_%s', name).to_sym
  end

end