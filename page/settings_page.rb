require_relative 'page'

class SettingsPage < Page

  def loopAllCategories
    getCategories.each do |category|
      loopCategory(category)
    end
  end

  def loopCategory(category)
    getCategoryElements(category).each do |element|
      validClickState(category, element)
    end
  end

  def validClickState(category, element)
    elementState = getElementAttributeValue(element, 'data-state', element: true)
    validClick(element, element: true)
    selectedState = getSelectedSettings(category)
    raise format('Category: %s, Selected: %s, Expected: %s', category, selectedState, elementState) unless (selectedState == elementState)
  end

  def clickState(category, state)
    index = @indexCSV.get(:selectors, self.class.name.to_sym, :settings_category_container, getBrandSelector)
    selectorIndex = format('%s[data-param=\'%s\'][data-state=\'%s\']', index, category, state)
    validClick(selectorIndex, index: false)
  end

  def getCategories
    [:injury, :game, :promotion]
  end

  def getCategoryElements(category, count: 4)
    index = @indexCSV.get(:selectors, self.class.name.to_sym, :settings_category_container, getBrandSelector)
    selectorIndex = format('%s[data-param=\'%s\']', index, category)
    getElementChildren(selectorIndex, count: count, index: false)
  end

  def getSelectedSettings(category)
    index = @indexCSV.get(:selectors, self.class.name.to_sym, :settings_category_container, getBrandSelector)
    selectorIndex = format('%s[data-param=\'%s\'] > div.settingsChecked', index, category)
    element = getElement(selectorIndex, index:false)
    elementParent = getElementParent(element, element: true)
    getElementAttributeValue(elementParent, 'data-state', element: true)
  end

end