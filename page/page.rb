require 'csv'
require 'timeout'
require 'active_support/time'
require_relative '../spec/spec_helper'
require 'continuation'
require "addressable/uri"

class Exception
  attr_accessor :continuation
  def ignore
    continuation.call
  end
end

module RaiseWithIgnore
  def raise(*args)
    callcc do |continuation|
      begin
        super
      rescue Exception => exception
        exception.continuation = continuation
        super(exception)
      end
    end
  end
end

class Object
  include RaiseWithIgnore
end

class Page

  def initialize(session, indexCSV, options = {})
    @session = session
    @indexCSV = indexCSV
  end

  def getBrandSelector
    format('%s_%s', Config.brand, Config.platform).to_sym
  end

  def getBrandLanguage
    format('%s_%s', Config.brand, Config.language).to_sym
  end

  def getBrandLocalTime(time, convert: nil)
    Time.zone = @indexCSV.get(:values, :Navigation, :brand_timezone, Config.brand)
    (convert.nil?) ? Time.parse(time).in_time_zone : Time.parse(time).in_time_zone.strftime(convert)
  end

  def getHostURL
    Addressable::URI.parse(@session.current_url).site
  end

  def getVisibilitySelectorName(selectorName, type, page: self.class.name.to_sym)
    selectorDisplay = format('%s_%s', selectorName, type).to_sym
    if(selectorName.to_s.include? "container")
      (selectorDisplay = selectorName.to_s.split(/(?<=_container)/).first.to_sym) unless (@indexCSV.exist(:selectors, page, selectorDisplay, getBrandSelector))
    end
    selectorDisplay
  end

  def validClosePopUp(wait: Capybara.default_max_wait_time, index: true)
    self.validClickAndDisappear(:popup_container_close_button, page: :PopUp, wait: wait, index: index)
  rescue Capybara::ElementNotFound => e
    (Config.platform == :mobile) ? (self.validClickMobileHome(wait: wait, index: index)) : (raise e)
  end
  
  def getElement(selectorName, element: false, page: self.class.name.to_sym, wait: Capybara.default_max_wait_time, index: true, visible: true, text: nil)
    unless(element)
      begin
        selectorIndex = getSelectorIndex(selectorName, page: page, index: index)
        @session.find(selectorIndex, visible: visible, text: text, wait: wait)
      rescue Capybara::ElementNotFound => exception
        (text.nil?) ? (raise exception) : (false)
      end
    else
      selectorName
    end
  end

  def getSelectorIndex(selectorName, page: self.class.name.to_sym, index: true)
    (index) ? (@indexCSV.get(:selectors, page, selectorName, getBrandSelector)) : (selectorName)
  end

  def getElementParent(selectorName, element: false, page: self.class.name.to_sym, wait: Capybara.default_max_wait_time, index: true)
    getElement(selectorName, element: element, page: page, wait: wait, index: index).find(:xpath, '..')
  end

  def getElementChildren(selectorName, element: nil, count: nil, page: self.class.name.to_sym, wait: Capybara.default_max_wait_time, index: true)
    if(element.nil?)
      selectorIndex = getSelectorIndex(selectorName, page: page, index: index)
      @session.all(selectorIndex, visible: true, count: count, wait: wait)
    else
      element.all(selectorName, visible: true, count: count, wait: wait)
    end
  end

  def getElementClass(selectorName, element: false, page: self.class.name.to_sym, wait: Capybara.default_max_wait_time, index: true)
    self.getElementAttributeValue(selectorName, 'class', element: element, page: page, wait: wait, index: index)
  end
  
  def getElementAttributeValue(selectorName, attributeName, element: false, page: self.class.name.to_sym, wait: Capybara.default_max_wait_time, index: true)
    value = getElement(selectorName, element: element, page: page, wait: wait, index: index)[attributeName]
    (value.nil?) ? (raise format('Element: %s, Attribute: %s not found', selectorName, attributeName)) : (value)
  end
  
  def getElementText(selectorName, element: false, page: self.class.name.to_sym, wait: Capybara.default_max_wait_time, index: true)
    self.validDisplay(selectorName, element: element, page: page, wait: wait, index: index)
    getElement(selectorName, element: element, page: page, wait: wait, index: index).text
  end

  def getElementValue(selectorName, element: false, page: self.class.name.to_sym, wait: Capybara.default_max_wait_time, index: true)
    getElement(selectorName, element: element, page: page, wait: wait, index: index).value
  end

  def validAttribute(selectorName, attributeName, value, element: false, page: self.class.name.to_sym, wait: Capybara.default_max_wait_time, index: true)
    attributeValue = getElementAttributeValue(selectorName, attributeName, element: element, page: page, wait: wait, index: index)
    raise format('Element: %s, Attribute Value: %s, Expected Value: %s', selectorName, attributeValue, value) unless (attributeValue == value)
  end
  
  def validClickAndDisplay(selectorName, element: false, page: self.class.name.to_sym, wait: Capybara.default_max_wait_time, index: true)
    self.validClick(selectorName, element: element, page: page, wait: wait, index: index)
    sleep 1
    self.validDisplayFromHash(selectorName, element: element, page: page, wait: wait, index: index)
  end

  def validClickAndDisappear(selectorName, element: false, page: self.class.name.to_sym, wait: Capybara.default_max_wait_time, index: true)
    self.validClick(selectorName, element: element, page: page, wait: wait, index: index)
    self.validDisappearFromHash(selectorName, element: element, page: page, wait: wait, index: index)
  end

  def validClickAndAlert(selectorName, alertAction, element: false, page: self.class.name.to_sym, wait: Capybara.default_max_wait_time, index: true)
    self.validClick(selectorName, element: element, page: page, wait: wait, index: index)
    sleep 0.5
    self.validAlertMessageFromHash(selectorName, alertAction, page: self.class.name.to_sym)
  end

  def validAlertMessageFromHash(selectorName, action, page: self.class.name.to_sym)
    selectorAction = format('%s_alert_%s', selectorName, action).to_sym
    messageIndex = @indexCSV.get(:messages, page, selectorAction, getBrandLanguage)
    self.validAlertMessage(messageIndex)
  end

  def validAlertMessage(message)
    alertMessage = self.validAlert(:text)
    self.validAlert(:accept)
    (raise format('Alert Text: %s, Expected Text: %s', alertMessage, message)) unless (alertMessage == message)
  end

  def validAlert(action = nil)
    alert = @session.driver.browser.switch_to.alert
    case action
      when :accept then alert.accept
      when :dismiss then alert.dismiss
      when :text then alert.text
      else alert
    end
  end

  def validClickMobileHome(wait: Capybara.default_max_wait_time, index: true)
    self.validClickAndDisplay(:home_lobby_button, page: :HomePage, wait: wait, index: index)
  end

  def validClick(selectorName, element: false, page: self.class.name.to_sym, wait: Capybara.default_max_wait_time, index: true)
    self.validDisplay(selectorName, element: element, page: page, wait: wait, index: index)
    getElement(selectorName, element: element, page: page, wait: wait, index: index).click
  end

  def validDisplayFromHash(selectorName, element: false, page: self.class.name.to_sym, wait: Capybara.default_max_wait_time, index: true)
    selectorDisplay = getVisibilitySelectorName(selectorName, :display, page: page)
    self.validDisplay(selectorDisplay, element: element, page: page, wait: wait, index: index)
  end

  def validDisplay(selectorName, element: false, page: self.class.name.to_sym, wait: Capybara.default_max_wait_time, index: true)
    getElement(selectorName, element: element, page: page, wait: wait, index: index).visible?
  rescue NoMethodError
    raise format('Element: %s, not visible', selectorName)
  end

  def validDisappearFromHash(selectorName, element: false, page: self.class.name.to_sym, wait: Capybara.default_max_wait_time, index: true)
    selectorDisappear = getVisibilitySelectorName(selectorName, :disappear, page: page)
    self.validDisappear(selectorDisappear, element: element, page: page, wait: wait, index: index)
  end

  def validDisappear(selectorName, element: false, page: self.class.name.to_sym, wait: Capybara.default_max_wait_time, index: true)
    element = getElement(selectorName, element: element, page: page, wait: wait, index: index, visible: false)
    Timeout::timeout(wait + 1) {(sleep 1) while (element.visible?)}
    rescue TimeoutError
      raise format('Element: %s, still visible', selectorName)
  end

  def validMessageFromHash(selectorName, action, element: false, page: self.class.name.to_sym, wait: Capybara.default_max_wait_time, index: true)
    selectorAction = format('%s_%s', selectorName, action).to_sym
    messageIndex = @indexCSV.get(:messages, page, selectorAction, getBrandLanguage)
    self.validMessage(selectorName, messageIndex, element: element, page: page, wait: wait, index: index)
  end

  def validMessage(selectorName, message, element: false, page: self.class.name.to_sym, wait: Capybara.default_max_wait_time, index: true)
    getElement(selectorName, element: element, page: page, text: message, wait: wait, index: index)
  rescue Capybara::ElementNotFound
    raise format('Element Text: %s, Expected Text: %s', getElementText(selectorName, element: element, page: page, wait: wait, index: index), message)
  end

  def validTextFieldFromHash(selectorName, value, element: false, page: self.class.name.to_sym, wait: Capybara.default_max_wait_time, index: true)
    selectorValue = format('%s_%s', selectorName, value).to_sym
    valueIndex = @indexCSV.get(:values, page, selectorValue, Config.brand)
    self.validTextField(selectorName, valueIndex, element: element, page: page, wait: wait, index: index)
  end

  def validTextField(selectorName, text, element: false, page: self.class.name.to_sym, wait: Capybara.default_max_wait_time, index: true)
    multiElements = nil
    begin
      self.validDisplay(selectorName, element: element, page: page, wait: wait, index: index)
    rescue Capybara::Ambiguous => exception
      multiElements = exception.message
    end
    if(getElement(selectorName, element: element, page: page, text: text, wait: wait, index: index) == false)
      case multiElements
        when nil
          raise format('Element Text: %s, Expected Text: %s', getElementText(selectorName, element: element, page: page, wait: wait, index: index), text)
        when String
          raise format('%s, Expected Text: %s', multiElements, text)
      end
    end
  end

  def validFillInputFromHash(selectorName, input, element: false, page: self.class.name.to_sym, wait: Capybara.default_max_wait_time, index: true)
    selectorInput = format('%s_%s', selectorName, input).to_sym
    valueIndex = @indexCSV.get(:values, page, selectorInput, Config.brand)
    self.validFillInput(selectorName, valueIndex, element: element, page: page, wait: wait, index: index)
  end

  def validFillInput(selectorName, sendKeys, clearInput: true, element: false, page: self.class.name.to_sym, wait: Capybara.default_max_wait_time, index: true)
    self.validDisplay(selectorName, element: element, page: page, wait: wait, index: index)
    clearInput ? (self.validInputClear(selectorName, element: element, page: page, wait: wait, index: index); inputText = sendKeys) : (inputText = format('%s%s', getElementValue(selectorName, page: page, wait: wait, index: index), sendKeys))
    getElement(selectorName, element: element, page: page, wait: wait, index: index).send_keys(sendKeys)
    validInputText(selectorName, inputText, element: element, page: page, wait: wait, index: index)
  end

  def validInputClear(selectorName, element: false, page: self.class.name.to_sym, wait: Capybara.default_max_wait_time, index: true)
    getElement(selectorName, element: element, page: page, wait: wait, index: index).send_keys([:control, 'a'], :delete)
    self.validInputText(selectorName, "", element: element, page: page, wait: wait, index: index)
  end

  def validInputText(selectorName, inputText, element: false, page: self.class.name.to_sym, wait: Capybara.default_max_wait_time, index: true)
    value = getElementValue(selectorName, element: element, page: page, wait: wait, index: index)
    (raise format('Selector: %s, Input Value: %s, SendKeys: %s', selectorName, inputText, value)) unless (value == inputText)
  end
  
  def vaildOpenAndDisplayWindow(selectorName, element: false, page: self.class.name.to_sym, wait: Capybara.default_max_wait_time, index: true)
    window = self.validOpenWindow(selectorName, element: element, page: page, wait: wait, index: index)
    window if self.validDisplayWindowFromHash(window, selectorName, element: element, page: page, wait: wait, index: index)
  end

  def validDisplayWindowFromHash(window, selectorName, element: false, page: self.class.name.to_sym, wait: Capybara.default_max_wait_time, index: true)
    selectorDisplay = getVisibilitySelectorName(selectorName, :display, page: page)
    self.validDisplayWindow(window, selectorDisplay, element: element, page: page, wait: wait, index: index)
  end

  def validOpenWindow(selectorName, element: false, page: self.class.name.to_sym, wait: Capybara.default_max_wait_time, index: true)
    window = @session.window_opened_by do
      self.validClick(selectorName, element: element, page: page, wait: wait, index: index)
    end
    @session.windows.include?(window) ? window : (raise format('Element: %s, fail to open window', selectorName))
  end

  def validCloseWindow(window)
    @session.within_window window do
      window.close
    end
    raise "Window still open" unless window.closed?
  end

  def validDisplayWindow(window, selectorName, element: false, page: self.class.name.to_sym, wait: Capybara.default_max_wait_time, index: true)
    @session.within_window window do
      self.validDisplay(selectorName, element: element, page: page, wait: wait, index: index)
    end
  end

  def validChooseAndValueSelectBox(selectorName, inputName, inputValue, element: false, page: self.class.name.to_sym, wait: Capybara.default_max_wait_time, index: true)
    self.validChooseSelectBox(selectorName, inputName, element: element, page: page, wait: wait, index: index)
    validSelectBoxValue(selectorName, inputValue, element: element, page: page, wait: wait, index: index)
  end

  def validChooseSelectBox(selectorName, inputName, element: false, page: self.class.name.to_sym, wait: Capybara.default, index: true)
    self.validDisplay(selectorName, element: element, page: page, wait: wait, index: index)
    getElement(selectorName, element: element, page: page, wait: wait, index: index).select(inputName)
  end

  def validSelectBoxValue(selectorName, inputValue, element: false, page: self.class.name.to_sym, wait: Capybara.default_max_wait_time, index: true)
    elementValue = getElementValue(selectorName, element: element, page: page, wait: wait, index: index)
    raise format('Element: %s, Value: %s, Expected Value: %s', selectorName, elementValue, inputValue) unless (elementValue == inputValue)
  end
  
end