require_relative '../page/page'

class SignupPage < Page

  def clickLogin
    self.validClickAndDisplay(:signup_login_button)
  end

  def clickFacebookSignup
    self.vaildOpenAndDisplayWindow(:signup_facebook_button)
  end

  def clickSignupSubmitButton
    self.validClick(:signup_submit_button)
  end

  def fillInputSignupFromHash(emailInputSelector, passwordInputSelector)
    self.validFillInputFromHash(:signup_email_input, emailInputSelector)
    self.validFillInputFromHash(:signup_password_input, passwordInputSelector)
  end

  def validFillInputSignup
    self.fillInputSignup(*getArraySignupEmailAndPassowrd)
  end

  def fillInputSignup(email, password)
    self.validFillInput(:signup_email_input, email)
    self.validFillInput(:signup_password_input, password)
  end

  def validSignupErrorMessage(action)
    self.validMessageFromHash(:signup_error_message, action).text
  end

  def getArraySignupEmailAndPassowrd
    [getSignupEmail, getSignupPassword]
  end

  def getSignupEmail
    (@signupEmail.nil?) ? (@signupEmail = getTimeStampEmail) : @signupEmail
  end

  def getTimeStampEmail
    timeStamp = Time.new.strftime("%m%d_%H%M%S")
    format('%s@fantasy65.com', timeStamp)
  end

  def getSignupPassword
    (@signupPassword.nil?) ? (@signupPassword = getTimeStampPassword) : @signupPassword
  end

  def getTimeStampPassword
    timeStamp = Time.new.strftime("%H%M%S")
    format('Qa%s!', timeStamp)
  end

end