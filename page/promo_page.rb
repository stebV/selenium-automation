require_relative 'page'

class PromoPage < Page

  def fillInputCode(sendKeys)
    self.validFillInput(:promo_code_input, sendKeys)
  end

  def clickSubmit
    self.validClick(:promo_submit_button)
  end

  def validCodeErrorMessage(action)
    self.validMessageFromHash(:promo_error_message, action)
  end

end