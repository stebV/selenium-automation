require_relative '../page/page'

class FacebookPage < Page

  def initialize(session, indexCSV, options = {})
    super(session, indexCSV)
    @facebook_window = options[:facebook_window]
  end

  def facebookLogin
    @session.within_window @facebook_window do
      self.fillInputFacebookFromHash(:valid, :valid)
      self.clickFacebookLoginButton
    end
  end

  def facebookSignup
    @session.within_window @facebook_window do
      self.validCloseWindow(@facebook_window)
    end
  end

  def clickFacebookLoginButton
    self.validClick(:facebook_login_button)
  end

  def fillInputFacebookFromHash(emailInputSelector, passwordInputSelector)
    self.validFillInputFromHash(:facebook_email_input, emailInputSelector)
    self.validFillInputFromHash(:facebook_password_input, passwordInputSelector)
  end

end