require_relative '../page/page'

class LoginPage < Page

  def clickSignup
    self.validClickAndDisplay(:login_signup_button)
  end

  def clickFacebookLogin
    self.vaildOpenAndDisplayWindow(:login_facebook_button)
  end

  def clickLoginSubmitButton
    self.validClick(:login_submit_button)
  end

  def fillInputLoginFromHash(emailInputSelector, passwordInputSelector)
    self.validFillInputFromHash(:login_email_input, emailInputSelector)
    self.validFillInputFromHash(:login_password_input, passwordInputSelector)
  end

  def fillInputLogin(email, password)
    self.validFillInput(:login_email_input, email)
    self.validFillInput(:login_password_input, password)
  end

  def validLoginErrorMessage(action)
    self.validMessageFromHash(:login_error_message, action).text
  end

  def clickForgotPasswordButton
    self.validClick(:login_forgot_password_button)
  end

  def clickForgotPasswordBackButton
    self.validClick(:login_forgot_password_back_button)
  end

  def clickForgotPasswordSendButton
    self.validClick(:login_forgot_password_send_button)
  end

  def fillInputForgotPassword(emailInputSelector)
    self.validFillInputFromHash(:login_forgot_password_email_input, emailInputSelector)
  end

  def validForgotPasswordErrorMessage(action)
    self.validMessageFromHash(:login_forgot_password_error_message, action)
  end

  def validForgotPasswordSendMessage
    self.validMessageFromHash(:login_forgot_password_send_message, :valid)
  end

end
