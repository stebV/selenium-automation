require_relative 'page'

class DepositPage < Page

  @selectedAmount

  def clickSubmit
    self.validClick(:deposit_submit_container_button)
  end

  def clickAndDisplaySubmit
    @selectedAmount = getSelectedAmount
    self.validClickAndDisplay(:deposit_submit_container_button)
  end

  def clickPaypalReturn
    self.validClick(:deposit_submit_container_paypal_return_button)
  end

  def validPaypalAmount
    paypalAmount = getPaypalAmount
    raise format('Element Amount: %s, PayPal Amount: %s', @selectedAmount, paypalAmount) unless (@selectedAmount == paypalAmount)
  end

  def getPaypalAmount
    amount = self.getElementText(:deposit_submit_container_paypal_amount_textfield).delete "$"
    (amount =~ /[0-9]/).nil? ? (raise format('PayPal Amount: %s', amount)) : amount.to_i
  end

  def loopAllAmount
    getAmountElements.each do |element|
      validTextAmount(element)
      validClickAmount(element)
    end
  end

  def getAmountElements
    getElementChildren(:deposit_amount_container, count: 4)
  end

  def validTextAmount(element)
    dataAmount = getDataAmount(element)
    elementText = getElementText(element, element: true).delete getCurrencySign(element)
    raise format('Element Text: %s, Data Amount: %s', elementText, dataAmount) unless (dataAmount == elementText)
  end

  def getDataAmount(element)
    getElementAttributeValue(element, 'data-amount', element: true)
  end

  def getCurrencySign(element)
    getElementChildren('span.currSign', element: element).first.text
  end

  def validClickAmount(element)
    validClick(element, element: true)
    selectedAmount = getSelectedAmount
    dataAmount = getDataAmount(element).to_i
    raise format('Amount Selected: %s, Expected Amount: %s', selectedAmount, dataAmount) unless (selectedAmount == dataAmount)
  end

  def getSelectedAmount
    selectedAmount = getElementAttributeValue(:deposit_amount_container_selected, "data-amount")
    (selectedAmount =~ /[0-9]/).nil? ? (raise format('Selected Amount: %s', selectedAmount)) : selectedAmount.to_i
  end

  def validDepositErrorMessage(action)
    self.validMessageFromHash(:deposit_error_message, action)
  end

  def validStateSelectBox(state, value)
    self.validChooseAndValueSelectBox(:deposit_state_selectbox, state, value)
  end

end