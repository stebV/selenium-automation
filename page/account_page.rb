require_relative 'page'

class AccountPage < Page

  def closeAccountPopUp
    case Config.platform
      when :web
        self.validClosePopUp
      when :mobile
        self.validClickMobileHome
    end
  end

  def redoDefaultUsername
    unless (defaultUsername == getUsernameTextField)
      clickChangeUsername
      fillInputChangeUsername(defaultUsername)
      clickChangeUsernameSave
    end
  end

  def defaultUsername
    @indexCSV.get(:values, self.class.name.to_sym, :account_username_textfield_default, Config.brand)
  end

  def clickDeposit
    self.validClickAndDisplay(:account_make_deposit_button)
  end

  def clickTransactions
    self.validClickAndDisplay(:account_transactions_button)
  end

  def clickChangeUsername
    self.validClickAndDisplay(:account_change_username_container_button)
  end

  def clickChangeUsernameSave(disappear: true)
    selectorName = :account_change_username_container_save_button
    disappear ? self.validClickAndDisappear(selectorName) : self.validClick(selectorName)
  end

  def clickChangeUsernameCancel
    self.validClickAndDisappear(:account_change_username_container_cancel_button)
  end

  def changeUsernameTemplate(template)
    clickChangeUsername
    fillInputChangeUsername(template, clearInput: false)
  end

  def fillInputChangeUsername(sendKeys, clearInput: true)
    self.validFillInput(:account_change_username_container_input, sendKeys, clearInput: clearInput)
  end

  def validAlertEmptyInput
    self.validInputClear(:account_change_username_container_input)
    self.validClickAndAlert(:account_change_username_container_save_button, :empty)
  end

  def validUsernameInputErrorMessage
    self.validMessageFromHash(:account_change_username_container_error_message, :boundary)
  end

  def validUsernameInputErrorMessageDisappear
    self.validDisappear(:account_change_username_container_error_message)
  end

  def clickWithdrawal
    self.validClickAndDisplay(:account_withdrawal_container_button)
  end

  def clickWithdrawalPaypal
    window = self.vaildOpenAndDisplayWindow(:account_withdrawal_container_paypal_button)
    self.validCloseWindow(window)
  end

  def clickWithdrawalUploadID
    self.validClickAndDisplay(:account_withdrawal_id_container_button)
  end

  def clickWithdrawalCancel
    self.validClickAndDisappear(:account_withdrawal_container_cancel_button)
  end

  def clickWithdrawalSubmit
    self.validClickAndDisappear(:account_withdrawal_container_submit_button)
  end

  def clickWithdrawalUploadIDCancel
    self.validClickAndDisappear(:account_withdrawal_id_container_cancel_button)
  end

  def fillInputWithdrawalAmount(sendKeys)
    self.validFillInput(:account_withdrawal_container_amount_input, sendKeys)
  end

  def validUsernameTextFieldFromHash
    self.validTextFieldFromHash(:account_username_textfield, :default)
  end

  def validUsernameTextField(username)
    self.validMessage(:account_username_textfield, username)
  end

  def getUsernameTextField
    self.getElementText(:account_username_textfield)
  end

  def validEmailTextFieldFromHash
    self.validTextFieldFromHash(:account_email_textfield, :default)
  end

  def validEmailTextField(email)
    self.validMessage(:account_email_textfield, email)
  end

  def getEmailTextField
    self.getElementText(:account_email_textfield)
  end

  def validSupportEmailLinkFromHash
    self.validTextFieldFromHash(:account_support_email_link, :default)
  end

  def validSupportEmailLink(email)
    self.validTextField(:account_support_email_link, email)
  end

  def getSupportEmailLink
    self.getElementText(:account_support_email_link)
  end

  def validBalanceTextField(amount)
    self.validTextField(:account_balance_textfield, amount)
  end

  def getBalanceTextField
    self.getElementText(:account_balance_textfield)
  end

  end