require_relative 'page'

class HomePage < Page

  def clickLobby
    self.validClickAndDisplay(:home_lobby_button)
  end

  def clickDepositPage
    self.validClickAndDisplay(:home_deposit_page_button)
  end

  def clickLoginPage
    self.validClickAndDisplay(:home_login_page_button)
  end

  def clickSignupPage
    self.validClickAndDisplay(:home_signup_page_button)
  end

  def clickAccountPage
    self.clickAccountMenu
    self.validClickAndDisplay(:home_account_page_button)
  end

  def clickSettingPage
    self.clickAccountMenu
    self.validClickAndDisplay(:home_setting_page_button)
  end

  def clickPromoPage
    self.clickAccountMenu
    self.validClickAndDisplay(:home_promo_page_button)
  end

  def clickReferralPage
    self.clickAccountMenu
    self.validClickAndDisplay(:home_referral_page_button)
  end

  def clickAccountLogout
    self.clickAccountMenu
    self.validClickAndDisplay(:home_account_signout_button)
  end

  def clickAccountMenu
    self.validClickAndDisplay(:home_account_menu_button)
  end

  def getAccountMenuUsername
    self.clickAccountMenu if Config.platform == :mobile
    username = self.getElementText(:home_account_menu_username_textfield)
    self.validClick(:home_account_menu_username_textfield) if Config.platform == :mobile
    username
  end

  def getBalanceAmount(amount: nil, wait: Capybara.default_max_wait_time)
    self.clickAccountMenu if Config.platform == :mobile
    sleep 4
    Timeout::timeout(wait + 1) {(amount = self.getElementText(:home_balance_textfield).delete "$"; sleep 1) while ((amount =~ /[0-9]/).nil?)}
    self.validClick(:home_balance_textfield)
    amount.to_i
  rescue TimeoutError
    raise format('Amount: %s', amount)
  end

end