require_relative 'page'
require 'net/http'
require 'json'
require 'date'
require "addressable/uri"
require 'active_support/core_ext/integer/inflections'
require "continuation"

class GamesPage < Page

  def initialize(*args)
    super(*args)
    initialHashActiveRooms
    @hashExceptions = Hash.new {|hash,key| hash[key] = []}
  end

  def initialHashActiveRooms
    tree_block = lambda{|hash, key| hash[key] = Hash.new(&tree_block)}
    @hashActiveRooms = Hash.new(&tree_block)
    hashRooms = getRoomsJSON
    getOpenGamesID.each do |leagueID, gameID|
      hashGame = getGameJsonByID(gameID)
      hashGame[:rooms] = hashRooms[gameID]
      hashGame[:categories] = getHashCategories(hashGame[:setups])
      hashGame[:matches] = getHashMatches(gameID)
      @hashActiveRooms[leagueID][gameID] = Hash[hashGame.collect {|key, value| [key, (value.kind_of?(Array) ? (mapArrayByID(value)) : (value))]}]
      @hashActiveRooms[leagueID][gameID][:active_categories] = getActiveCategories(leagueID, gameID, hashRooms[gameID])
    end
  end

  def getActiveCategories(leagueID, gameID, hashRoom)
    set = Set.new
    hashRoom.each { |room| set.add(getCategoryNameByID(leagueID, gameID, room[:setup_id])) }
    (Config.platform == :web) ? set.add("VIEW ALL") : set
  end

  def getCategoryRooms(leagueID, gameID, categoryID)
    if(categoryID == 0)
      @hashActiveRooms.dig(leagueID, gameID, :rooms)
    else
      setups = getCategorySetupsByID(leagueID, gameID, categoryID)
      Hash[@hashActiveRooms.dig(leagueID, gameID, :rooms).find_all {|key, value| setups.include? value[:setup_id] }]
    end
  end

  def getFilterIndex(categoryID)
    index = @indexCSV.get(:selectors, self.class.name.to_sym, :games_category_filter_container, getBrandSelector)
    categoryID = categoryID.to_s.prepend("$") unless (categoryID == 0)
    (format('%s[data-reactid*=\'cat.%s\']', index, categoryID))
  end

  def validClickCategoryFilter(leagueID, gameID, categoryID)
    indexFilter = getFilterIndex(categoryID)
    self.validClick(indexFilter, index: false)
    filterCategory = getCategoryNameByID(leagueID, gameID, categoryID)
    fragmentCategory, fragmentLeagueID = getFragmentValue.values_at(:id, :leaugeId)
    selectedFilter = ((Config.platform == :web) ? (getSelectedFilter) : (fragmentCategory))
    raise format('Selected: %s, Fragment: %s, Expected: %s', selectedFilter,fragmentCategory, filterCategory) unless ([selectedFilter, fragmentCategory, filterCategory].uniq.count == 1)
    raise format('Fragment LeagueID: %s, Expected: %s', fragmentLeagueID, leagueID) unless (fragmentLeagueID == leagueID)
  end

  def getSelectedFilter
    self.getElementText(:games_category_filter_container_selected)
  end

  def loopLeagueContainer
    getLeagueContainerElements.each do |leagueContainerElement|
      validClickLeagueSelection(leagueContainerElement)
      leagueContainerName = getLeagueContainerNameByElement(leagueContainerElement)
      activeLeagueID = getActiveLeagueIdByName(leagueContainerName)
      (activeLeagueID.nil?) ? (validNoRoomsDisplay) : (loopCategoriesFilterContainer(activeLeagueID))
    end
    raise format('Loop fail: %s', @hashExceptions.to_s) unless @hashExceptions.empty?
  end

  def loopCategoriesFilterContainer(leagueID)
    @hashActiveRooms[leagueID].each do |gameID, gameHash|
      getActiveLeagueCategoriesID(leagueID, gameID).each do |categoryID|
        loopRoomContainer(leagueID, gameID, categoryID)
      end
    end

  end

  def getActiveLeagueCategoriesID(leagueID, gameID)
    @hashActiveRooms.dig(leagueID, gameID, :active_categories).collect { |categoryName| getCategoryIdByName(leagueID, gameID, categoryName) }
  end

  def getCategoryIdByName(leagueID, gameID, categoryName)
    @hashActiveRooms.dig(leagueID, gameID, :categories, categoryName).min
  end

  def loopRoomContainer(leagueID, gameID, categoryID)
    validClickCategoryFilter(leagueID, gameID, categoryID)
    categoryRooms = getCategoryRooms(leagueID, gameID, categoryID)
    loopRoomContainerFields(leagueID, categoryRooms)
    randomRoomID = categoryRooms.keys.sample
    validRoomInformationContainer(leagueID, gameID, randomRoomID)
    validClickBackToCategories(categoryID) if (Config.platform == :mobile)
  end

  def loopRoomContainerFields(leagueID, categoryRooms)
    categoryRooms.each do |roomID ,room|
      begin
        validRoomContainerFields(leagueID, room)
      rescue RuntimeError, Capybara::ElementNotFound => exception
        @hashExceptions[roomID].push(exception.message)
        exception.ignore
      end
    end
  end

  def validRoomContainerFields(leagueID, room, container: :room)
    validRoomContainerPrize(leagueID, room, container: container)
    validRoomContainerFee(leagueID, room, container: container)
    if(Config.platform == :web)
      validRoomContainerName(leagueID, room, container: container)
      validRoomContainerCategory(leagueID, room, container: container)
    end
  end

  def validRoomInformationContainer(leagueID, gameID, roomID)
    room = getRoomHashByID(leagueID, gameID, roomID)
    validClickInformationButton(roomID)
    begin
      validRoomContainerFields(leagueID, room, container: :information)
      validRoomInformationPayouts(leagueID, gameID, room[:setup_id])
      validRoomInformationMatches(leagueID, gameID)
      jsonEntries = validRoomInformationEntries(roomID)
      validRoomInformationParticipants(jsonEntries, room[:room_size])
    rescue RuntimeError, Capybara::ElementNotFound => exception
      @hashExceptions[roomID].push(exception.message)
      exception.ignore
    end
    validCloseRoomInformation
  end

  def validRoomInformationPayouts(leagueID, gameID, setupID)
    getSortSetupPrizeLevel(leagueID, gameID, setupID).each_with_index do|payoutString, index|
      self.validTextField(getPayoutsIndex(index), payoutString, index: false, wait: 0)
    end
  end

  def getSortSetupPrizeLevel(leagueID, gameID, setupID)
    currencySign = getCurrencySign(leagueID, gameID, setupID)
    hash = Hash.new {|hash,key| hash[key] = []}
    getSetupPrizeLevelByID(leagueID, gameID, setupID).each_with_index do |prize, index|
      hash[prize].push(index + 1)
    end
    hash.collect { |prize, arrayIndexes| getPayoutString(prize, currencySign, *arrayIndexes.minmax) }
  end

  def getPayoutsIndex(index)
    indexContainer = @indexCSV.get(:selectors, self.class.name.to_sym, :games_information_container_payouts, getBrandSelector)
    indexContainer.gsub(/payout/) { |pattern| "#{pattern}#{index}" }
  end

  def getSetupPrizeLevelByID(leagueID, gameID, setupID)
    @hashActiveRooms.dig(leagueID, gameID, :setups, setupID, :prize_levels)
  end

  def getPayoutString(prize, currencySign, firstPlace, lastPlace)
    place = (firstPlace == lastPlace) ? (lastPlace.ordinalize) : (format('%s - %s', firstPlace.ordinalize, lastPlace.ordinalize))
    format('%s %s%s', place, currencySign, prize)
  end

  def validRoomInformationMatches(leagueID, gameID)
    @hashActiveRooms.dig(leagueID, gameID, :matches).each do |matchID, hashMatch|
      hashMatch.each { |key, value| self.validTextField(getMatchIndex(key, matchID), value, wait: 0, index: false) }
    end
  end

  def getMatchIndex(key, matchID)
    selectorName = format('games_information_container_match_%s', key.to_s.split('_').last).to_sym
    index = @indexCSV.get(:selectors, self.class.name.to_sym, selectorName, getBrandSelector)
    format('%s[data-reactid*=\'%s\']', index, matchID)
  end

  def validRoomInformationParticipants(jsonEntries, jsonRoomSize)
    roomEntries, roomSize  = getRoomInformationParticipants
    (raise format('Room: %s/%s, JSON: %s/%s', roomEntries, roomSize, jsonEntries, jsonRoomSize)) unless (roomEntries == jsonEntries && roomSize == jsonRoomSize)
  end

  def getRoomInformationParticipants
    getElementText(:games_information_container_participants).split("/").collect { |item| item.to_i }
  end

  def validRoomInformationEntries(roomID)
    arrayUsersEntries = getRoomUsersJSON(roomID)
    arrayUsersEntries.first(30).each_with_index do |userEntry, indexEntry|
      selectorIndex = getUserEntryIndex(indexEntry)
      validTextField(selectorIndex, userEntry, wait: 0, index: false)
    end
    arrayUsersEntries.length
  end

  def getUserEntryIndex(indexEntry)
    index = @indexCSV.get(:selectors, self.class.name.to_sym, :games_information_container_entry, getBrandSelector)
    (format('%s[data-reactid$=\'%s\']', index, indexEntry))
  end

  def validClickInformationButton(roomID)
    validClickPlayerSelectionButton(roomID) if (Config.platform == :mobile)
    selectorIndex = getContainerIndex(:button, roomID, container: :information)
    self.validClick(selectorIndex, index: false)
    self.validDisplayFromHash(:games_information_container)
  end

  def validClickPlayerSelectionButton(roomID)
    selectorIndex = getContainerIndex(:button, roomID, container: :player_selection)
    self.validClick(selectorIndex, index: false)
    self.validDisplayFromHash(:games_player_selection_container)
  end

  def validClickBackToCategories(categoryID)
    self.validClick(:games_category_filter_container_back_button)
    selectorIndex = getContainerIndex(nil, categoryID, container: :category_filter)
    self.validDisplay(selectorIndex, index: false)
  end

  def validCloseRoomInformation
    self.validClickAndDisappear(:games_information_container_close_button)
    @session.go_back if (Config.platform == :mobile)
  end

  def getActiveLeagueIdByName(leagueName)
    result = @hashActiveRooms.detect {|key, value| getLeagueNameByID(key) == leagueName}
    result.first unless (result.nil?)
  end

  def getLeagueContainerNameByElement(element)
    self.getElementText(element, element: true)
  end

  def validClickLeagueSelection(element)
    self.validClick(element, element: true)
    fragmentLeagueName = getLeagueNameByID(getFragmentValue)
    leagueContainerName = getLeagueContainerNameByElement(element)
    raise format('Fragment: %s, Element: %s', fragmentLeagueName, leagueContainerName) unless (fragmentLeagueName == leagueContainerName)
  end

  def getFragmentValue
    fragment = Addressable::URI.parse(@session.current_url).fragment
    array = fragment.split('?')
    case array.first
      when "leauge"
        array.last
      when "category"
        JSON.parse(array.last, symbolize_names: true).collect {|key, value| (value =~ /^[0-9]+$/ ) ? ([key, value.to_i]) : ([key, value.upcase])}.to_h
      else raise format('Fragment: %s, no case for this value', array.first)
    end
  end

  def validNoRoomsDisplay
    self.validDisplay(:games_league_container_empty)
  end

  def getLeagueContainerElements
    self.getElementChildren(:games_league_container)
  end

  def getContainerIndex(field, roomID, container: :room)
    selectorName = ((field.nil?) ? (format('games_%s_container', container).to_sym) : (format('games_%s_container_%s', container, field).to_sym))
    index = @indexCSV.get(:selectors, self.class.name.to_sym, selectorName, getBrandSelector)
    (containerIndexCases(field, container)) ? (format('%s[data-reactid*=\'%s\']', index, roomID)) : (index)
  end

  def containerIndexCases(field, container)
    case container
      when :information then (field == :button && Config.platform == :web) ? (true) : (false)
      else true
    end
  end

  def validRoomContainerCategory(leagueID, room, container: :room)
    roomContainerCategory = getRoomContainerCategory(room[:id], container: container)
    roomCategory = getRoomCategory(leagueID, room[:game_id], room[:setup_id])
    roomCategory = "PRACTICE" if (Config.brand == :brand_24manager && roomCategory == "FREE")
    raise format('RoomID: %s, Room Category: %s, JSON Category: %s', room[:id], roomContainerCategory, roomCategory) unless (roomContainerCategory.casecmp(roomCategory).zero?)
  end

  def getRoomContainerCategory(roomID, container: :room)
    selectorIndex = getContainerIndex(:category_textfield, roomID, container: container)
    self.getElementText(selectorIndex, index: false)
  end

  def getRoomCategory(leagueID, gameID, setupID)
    @hashActiveRooms.dig(leagueID, gameID, :setups, setupID)[:category].upcase
  end

  def validRoomContainerFee(leagueID, room, container: :room)
    currency = getCurrencySign(leagueID, room[:game_id], room[:setup_id])
    roomContainerFee = getRoomContainerFee(room[:id], currency, container: container)
    roomFee = getRoomFee(room)
    raise format('Room: %s, Room Fee: %s', room[:id], roomContainerFee) unless (roomContainerFee == roomFee)
  end

  def getRoomFee(room)
    room[:price] / 100
  end

  def getRoomContainerFee(roomID, currency, container: :room)
    selectorIndex = getContainerIndex(:fee_textfield, roomID, container: container)
    fee = self.getElementText(selectorIndex, index: false).delete "#{currency},"
    (fee =~ /^[0-9]+$/).nil? ? (raise format('Room: %s, Fee: %s', roomID, fee)) : fee.to_i
  end

  def validRoomContainerPrize(leagueID, room, container: :room)
    currency = getCurrencySign(leagueID, room[:game_id], room[:setup_id])
    roomContainerPrize = getRoomContainerPrize(room[:id], currency, container: container)
    roomPrize = getRoomPrize(room)
    raise format('Room: %s, Room Prize: %s', room[:id], roomContainerPrize) unless (roomContainerPrize == roomPrize)
  end

  def getCurrencySign(leagueID, gameID, setupID)
    hashSign = {"USD" => "$"}
    hashSign[@hashActiveRooms.dig(leagueID, gameID, :setups, setupID)[:currency]]
  end

  def getRoomContainerPrize(roomID, currency, container: :room)
    selectorIndex = getContainerIndex(:prize_textfield, roomID, container: container)
    prize = self.getElementText(selectorIndex, index: false).delete "#{currency},"
    (prize =~ /^[0-9]+$/).nil? ? (raise format('Room: %s, Prize: %s', roomID, prize)) : prize.to_i
  end

  def getRoomPrize(room)
    room[:prize_amount] / 100
  end

  def validRoomContainerName(leagueID, room, container: :room)
    return nil if (Config.brand == :brand_24manager && container == :room)
    roomContainerName = getRoomContainerNameByID(room[:id], container: container)
    gameName = getGameNameByID(leagueID, room[:game_id])
    raise format('Room Name: %s, Game Name: %s', room[:id], roomContainerName, gameName) unless (roomContainerName.casecmp(gameName).zero?)
    end

  def getRoomContainerNameByID(roomID, container: :room)
    selectorIndex = getContainerIndex(:name_textfield, roomID, container: container)
    self.getElementText(selectorIndex, index: false)
  end

  def getGameNameByID(leagueID, gameID)
    @hashActiveRooms.dig(leagueID, gameID, :name)
  end

  def deleteUpcomingLeagues(leagueID, gameStartTime)
    (@hashActiveRooms[leagueID].delete_if{|key, value| value[:start_time] > gameStartTime }) if (@hashActiveRooms.has_key? leagueID)
  end

  def mapArrayByID(array)
    Hash[array.collect { |item| [item[:id], item] }]
  end

  def getHashCategories(gameSetups)
    hash = Hash.new {|hash,key| hash[key] = []}
    (hash['VIEW ALL'].push(0)) if (Config.platform == :web)
    gameSetups.each { |setup| hash[setup[:category].upcase].push(setup[:id]) }
    hash
  end

  def getCategoryNameByID(leagueID, gameID, categoryID)
    @hashActiveRooms.dig(leagueID, gameID, :categories).detect {|key, value| value.include? categoryID}.first.upcase
  end

  def getCategorySetupsByID(leagueID, gameID, categoryID)
    @hashActiveRooms.dig(leagueID, gameID, :categories).detect {|key, value| value.include? categoryID}.last
  end

  def getOpenGamesID
    activeLeagues = getActiveLeaguesSortByBrand
    Hash[activeLeagues.collect { |leagueID, league| [leagueID, getUpcomingGameID(league)]}]
  end

  def getUpcomingGameID(league)
    upcomingGame = league.min_by(1) {|game| getStartTimeDateObject(game[:start_time])}
    (upcomingGame.first[:id]) unless (upcomingGame.nil?)
  end

  def getActiveLeaguesSortByBrand
    Hash[getActiveLeagues.find_all {|leagueID, league| getBrandLeagues.include? leagueID}]
  end

  def getActiveLeagues
    getGamesJSON.group_by { |game| game[:prototype_id]}
  end

  def getGamesJSON
    url = format('%s%s', getHostURL, @indexCSV.getJsonURL(:games, self.class.name))
    @indexCSV.getApiJson(url)
  end

  def getGameJsonByID(gameID)
    url = format('%s%s/%s', getHostURL, @indexCSV.getJsonURL(:games, self.class.name), gameID.to_s)
    @indexCSV.getApiJson(url)
  end

  def getRoomsJSON
    url = format('%s%s', getHostURL, @indexCSV.getJsonURL(:rooms, self.class.name))
    @indexCSV.getApiJson(url).group_by { |room| room[:game_id]}
  end

  def getRoomUsersJSON(roomID)
    url = format('%s/v1/public/room/rooms/%s/users', getHostURL, roomID)
    @indexCSV.getApiJson(url).collect { |user| user[1] }
  end

  def getHashMatches(gameID)
    hashTeams = getTeamsJSON(gameID)
    Hash[getMatchesJSON(gameID).collect { |hashMatch| [hashMatch[:id], sortMatchHash(hashTeams, hashMatch)] }]
  end

  def getMatchesJSON(gameID)
    urlMatches = format('%s/v1/public/metadata/games/%s/matches', getHostURL, gameID)
    @indexCSV.getApiJson(urlMatches)
  end

  def getTeamsJSON(gameID)
    urlTeams = format('%s/v1/public/metadata/games/%s/teams', getHostURL, gameID)
    Hash[@indexCSV.getApiJson(urlTeams).collect { |item| [item[:id], item[:name].delete(".")] }]
  end

  def sortMatchHash(hashTeams, hashMatch)
    homeTeamID, visitingTeamID, startTime = hashMatch.values_at(:home_team_id, :visiting_team_id, :start_time)
    startDate = getBrandLocalTime(startTime, convert: '%a, %-l%p EST')
    { home_team: hashTeams[homeTeamID], visiting_team: hashTeams[visitingTeamID], start_date: startDate }
  end

  def getBrandLeagues
    @indexCSV.get(:league, Config.brand)
  end

  def getLeagueNameByID(id)
    @indexCSV.get(:league, :league_list, id.to_s.to_sym)
  end

  def getStartTimeDateObject(startTime)
    Date::strptime(startTime, "%Y-%m-%d")
  end

  def getRoomHashByID(leagueID, gameID, roomID)
    @hashActiveRooms.dig(leagueID, gameID, :rooms, roomID)
  end

end