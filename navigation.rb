require 'accessor_extender'
require_relative 'page/login_page'
require_relative 'page/signup_page'
require_relative 'page/home_page'
require_relative 'page/facebook_page'
require_relative 'page/account_page'
require_relative 'page/deposit_page'
require_relative 'page/settings_page'
require_relative 'page/promo_page'
require_relative 'page/referral_page'
require_relative 'page/games_page'
require_relative 'csv/builder_csv'
require_relative 'config'

class Navigation

  def initialize
    Config.brand = :brand_drafthero
    Config.environment = :production
    Config.platform = :web
    Config.language = :en
    @indexCSV = BuilderCSV.new
    Capybara.default_driver = :chrome
    Capybara.app_host = getBaseUrl
    @session = Capybara::Session.new(Capybara.default_driver)
    @hashPageObject = {}
  end

  def getBaseUrl
    baseUrl = format('base_url_%s_%s', Config.platform, Config.environment).to_sym
    @indexCSV.get(:values, self.class.name.to_sym, baseUrl, Config.brand)
  end

  def goToHomePage
    @session.visit("?lang=#{Config.language}")
    getPageObject(:HomePage)
  end

  def goToDepositPage
    getPageObject(:HomePage).clickDepositPage
    getPageObject(:DepositPage)
  end

  def goToLoginPage
    getPageObject(:HomePage).clickLoginPage
    getPageObject(:LoginPage)
  end

  def goToSignupPage
    getPageObject(:HomePage).clickSignupPage
    getPageObject(:SignupPage)
  end

  def goToFacebookPage
    getPageObject(:HomePage).clickLoginPage
    getPageObject(:FacebookPage, facebook_window: getPageObject(:LoginPage).clickFacebookLogin)
  end

  def goToAccountPage
    getPageObject(:HomePage).clickAccountPage
    getPageObject(:AccountPage)
  end

  def goToSettingPage
    getPageObject(:HomePage).clickSettingPage
    getPageObject(:SettingsPage)
  end

  def goToPromoPage
    getPageObject(:HomePage).clickPromoPage
    getPageObject(:PromoPage)
  end

  def goToReferralPage
    getPageObject(:HomePage).clickReferralPage
    getPageObject(:ReferralPage)
  end

  def goToGamesPage
    # getPageObject(:HomePage).clickLobby
    getPageObject(:GamesPage)
  end

  def getPageObject(page, options={})
    @hashPageObject.key?(page) ? @hashPageObject[page] : @hashPageObject[page] = Object.const_get(page).new(@session, @indexCSV, options)
  end

  def resetSession
    @session.reset!
  end

  def refreshPage
    @session.driver.browser.navigate.refresh
  end

  def goBack
    @session.go_back
  end

  def clickAlert
    @session.driver.browser.switch_to.alert.accept
  end

  def getAlertText
    @session.driver.browser.switch_to.alert.text
  end

end
