require 'csv'
require "addressable/uri"

# require_relative '../navigation'

class BuilderCSV
  def initialize
    initialHashCSV
    initialHashJSON
  end

  def initialHashCSV
    @hashCSV = {}
    getFilesPathCSV.each do |filePath|
      @hashCSV[getFileNameSymbol(filePath, :csv)] = parseCSV(filePath)
    end
  end

  def initialHashJSON
    @hashJSON = {}
    getFilesPathJSON.each do |filePath|
      jsonInput = File.read(filePath)
      @hashJSON[getFileNameSymbol(filePath, :json)] = parseJSON(jsonInput)
    end
  end

  def get(*fields)
    value = @hashCSV.dig(*fields)
    value = @hashJSON.dig(*fields) if (value.nil? || value.empty?)
    (value.nil? || value.empty?) ? (raise getRaiseMessage(*fields)) : value
  end

  def getRaiseMessage(*fields)
    path = fields.join(' > ')
    format('Dig Path: %s, not exist', path)
  end

  def exist(*fields)
    value = @hashCSV.dig(*fields)
    value = @hashJSON.dig(*fields) if (value.nil? || value.empty?)
    (value.nil? || value.empty?) ? false : true
  end

  def getFilesPathCSV
    Dir.glob("./csv/*.csv")
  end

  def getFilesPathJSON
    Dir.glob("./csv/*.json")
  end

  def getFileNameSymbol(filePath, type)
    File.basename(filePath, ".#{type}").to_sym
  end

  def parseCSV(path)
    tree_block = lambda{|hash, key| hash[key] = Hash.new(&tree_block)}
    hash = Hash.new(&tree_block)
   CSV.foreach(path, headers: true, header_converters: :symbol) do |row|
      hashBrands = row.to_hash.keep_if {|key| key[/brand/]}
     hash[row[:page].to_sym][row[:name].to_sym] = hashBrands
    end
    hash
  end

  def parseJSON(jsonInput)
    JSON.parse(jsonInput, symbolize_names: true)
  end

  def getApiJson(jsonURL)
    jsonInput = getResponseHTTP(getJsonURI(jsonURL))
    parseJSON(jsonInput)
  end

  def getResponseHTTP(uri)
    response = Net::HTTP.get(uri)
    (response.nil? || response.empty?) ? (raise format('URI: %s, empty', response)) : (response)
  end

  def getJsonURI(jsonURL)
    Addressable::URI.parse(jsonURL)
  end

  def getJsonURL(index, page)
    self.get(:values, page.to_sym, getJsonIndex(index), Config.brand)
  end

  def getJsonIndex(index)
    format('json_url_%s', index).to_sym
  end

end